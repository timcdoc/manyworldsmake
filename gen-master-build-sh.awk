BEGIN {
    FULL=2
    STHREAD=2
    forbidden["full","st"]++
    forbidden["master-ary","mt"]++
    aallocators[1]="MAKE_NOBRICK=1 MAKE_ARRAYALLOC=1 "
    aallocators[2]="MAKE_NOBRICK=1"
    aallocators[3]="MAKE_NOBRICK=1 MAKE_TYPELESS=1"
    aallocators[0]=3
    aallocatorstext[1]="master-ary"
    aallocatorstext[2]="master"
    aallocatorstext[3]="master-tyless"
    aallocatorstext[0]=3
    adebugrelease[1]="MAKE_DEBUG=debug"
    adebugrelease[2]="MAKE_DEBUG=debug MAKE_DEBUGLISPSTACK=1"
    adebugrelease[3]="MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1"
    adebugrelease[4]="MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1 MAKE_DEBUGLISPSTACK=1"
    adebugrelease[5]="MAKE_STRIP=1"
    adebugrelease[0]=5
    adebugreleasetext[1]="dbg"
    adebugreleasetext[2]="dbg-lisp"
    adebugreleasetext[3]="dbg-mem"
    adebugreleasetext[4]="dbg-mem-lisp"
    adebugreleasetext[5]="rel"
    adebugreleasetext[0]=5
    abasefull[1]="MAKE_X11=1"
    abasefull[2]=" "
    abasefull[0]=2
    abasefulltext[1]="full"
    abasefulltext[2]="base"
    abasefulltext[0]=2
    asthread[1]=" "
    asthread[2]="MAKE_STHREAD=1"
    asthread[0]=2
    asthreadtext[1]="mt"
    asthreadtext[2]="st"
    asthreadtext[0]=2
    fn_case32 = "case-master-32.sh"
    fn_options32 = "options-master-32.sh"
    fn_case64 = "case-master-64.sh"
    fn_options64 = "options-master-64.sh"
    printf( "#autogenerated from awk -f gen-master-build-sh.awk\ncase \"$1\" in\n" ) >fn_case32
    close( fn_case32 )
    printf( "#autogenerated from awk -f gen-master-build-sh.awk\ncase \"$1\" in\n" ) >fn_case64
    close( fn_case64 )
    printf( "#autogenerated from awk -f gen-master-build-sh.awk\n" ) >fn_options32
    close( fn_options32 )
    printf( "#autogenerated from awk -f gen-master-build-sh.awk\n" ) >fn_options64
    close( fn_options64 )
    for ( allocators = 1; allocators <= aallocators[0]; allocators++ ) {
        for ( debugrelease = 1; debugrelease <= adebugrelease[0]; debugrelease++ ) {
            for ( basefull = 1; basefull <= abasefull[0]; basefull++ ) {
                for ( sthread = 1; sthread <= asthread[0]; sthread++ ) {
            
                    if ( !( (abasefulltext[basefull],asthreadtext[sthread]) in forbidden ) &&
                     !( (aallocatorstext[allocators],asthreadtext[sthread]) in forbidden ) ) {
                        copt++
                        printf( "options[%d]=\"%s 32 bit %s %s %s\"\n", copt, aallocatorstext[allocators], adebugreleasetext[debugrelease], abasefulltext[basefull], asthreadtext[sthread] ) >>fn_options32
                        close( fn_options32 )
                        printf( "options[%d]=\"%s 32 bit %s %s %s\"\n", copt, aallocatorstext[allocators], adebugreleasetext[debugrelease], abasefulltext[basefull], asthreadtext[sthread] ) >>fn_options64
                        close( fn_options64 )
                        printf( "%d) unset $unsetARGS; export MAKE_BITNESS=32 %s %s %s %s; make_wrapper;;\n", copt, aallocators[allocators], adebugrelease[debugrelease], abasefull[basefull], asthread[sthread] ) >>fn_case32
                        close( fn_options32 )
                        printf( "%d) unset $unsetARGS; export MAKE_BITNESS=32 %s %s %s %s; make_wrapper;;\n", copt, aallocators[allocators], adebugrelease[debugrelease], abasefull[basefull], asthread[sthread] ) >>fn_case64
                        close( fn_options64 )
                        if ( adebugreleasetext[debugrelease] == "rel" ) {
                            footprinttext = "" footprinttext " " copt
                        }
                    }
                }
            }
        }
    }
    printf( "options[%d]=\"footprint warning, deletes all builds first\"\n", copt+1 ) >>fn_options32
    close( fn_options32 )
    printf( "options[%d]=\"all\"\n", copt+2 ) >>fn_options32
    close( fn_options32 )
    printf( "%d) make clean-all-builds\nfor j in %s\ndo\ndo_command $j\ndone;;\n", copt+1, footprinttext ) >>fn_case32
    close( fn_case32 )
    printf( "%d) for (( j = 1; j <= %d; j++ ))\ndo\ndo_command $j\ndone;;\n", copt+2, copt ) >>fn_case32
    close( fn_case32 )
    printf( "esac\n" ) >>fn_case32
    close( fn_case32 )
    for ( allocators = 1; allocators <= aallocators[0]; allocators++ ) {
        for ( debugrelease = 1; debugrelease <= adebugrelease[0]; debugrelease++ ) {
            for ( basefull = 1; basefull <= abasefull[0]; basefull++ ) {
                for ( sthread = 1; sthread <= asthread[0]; sthread++ ) {
        
                    if ( !( (abasefulltext[basefull],asthreadtext[sthread]) in forbidden ) &&
                     !( (aallocatorstext[allocators],asthreadtext[sthread]) in forbidden ) ) {
                        copt++
                        printf( "options[%d]=\"%s 64 bit %s %s %s\"\n", copt, aallocatorstext[allocators], adebugreleasetext[debugrelease], abasefulltext[basefull], asthreadtext[sthread] ) >>fn_options64
                        close( fn_options64 )
                        printf( "%d) unset $unsetARGS; export %s %s %s %s; make_wrapper;;\n", copt, aallocators[allocators], adebugrelease[debugrelease], abasefull[basefull], asthread[sthread] ) >>fn_case64
                        close( fn_options64 )
                        if ( adebugreleasetext[debugrelease] == "rel" ) {
                            footprinttext = "" footprinttext " " copt
                        }
                    }
                }
            }
        }
    }
    printf( "options[%d]=\"footprint warning, deletes all builds first\"\n", copt+1 ) >>fn_options64
    close( fn_options64 )
    printf( "options[%d]=\"all\"\n", copt+2 ) >>fn_options64
    close( fn_options64 )
    printf( "%d) make clean-all-builds\nfor j in %s\ndo\ndo_command $j\ndone;;\n", copt+1, footprinttext ) >>fn_case64
    close( fn_case64 )
    printf( "%d) for (( j = 1; j <= %d; j++ ))\ndo\ndo_command $j\ndone;;\n", copt+2, copt ) >>fn_case64
    close( fn_case64 )
    printf( "esac\n" ) >>fn_case64
    close( fn_case64 )
}

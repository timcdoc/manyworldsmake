#!/bin/bash
unset options i
MACHINE=`uname -m`
BRANCH=`git rev-parse --abbrev-ref HEAD`
OSTYPE=`( [ -z $OSTYPE ] && echo 'windows_nt' ) || echo $OSTYPE | sed 's/-.*//'`
unsetARGS="MAKE_DEBUG MAKE_DEBUGLISPSTACK MAKE_NOBRICK MAKE_ALSA MAKE_BRICK_ATOMIC MAKE_X11 MAKE_BITNESS MAKE_TYPELESS MAKE_DEBUG_MEMORY MAKE_STHREAD MAKE_ARRAYALLOC ARCH MAKE_TOMSARRAY MAKE_STRIP MAKE_ZSTRINGS MAKE_ZATOMS"

if [[ "$OSTYPE" == "cygwin" ]]
  then
      MAKEFULL="MAKE_X11=1"
  else
      MAKEFULL="MAKE_X11=1 MAKE_ALSA=1"
fi
if [[ "$BRANCH" == "alloc" ]]
  then
    if [[ "$OSTYPE" == "cygwin" ]]
      then
          source "options-alloc-cygwin.sh"
      else
          source "options-alloc.sh"
    fi
  else
    source "options-master.sh"
fi

make_wrapper ()
{
   local buillddir
   local j
   builddir=`make --no-print-directory -s echobuilddir`
#   echo cleaning $builddir
   make --no-print-directory -s clean
   echo making $builddir lisp,make.log,make.err
   if make $1 --no-print-directory all >$builddir/make.log 2>$builddir/make.err
   then
       j=0
#       echo Build succeeded $builddir
   else
       echo Build failed with errors $builddir/make.err
       grep error $builddir/make.err
       grep "error: gnu/stubs-32.h:" $builddir/make.err && dnf install glibc-devel.i686
       grep "cannot find -lX11" $builddir/make.err && dnf install libX11.i686 libX11-devel.i686
       grep "cannot find -lasound" $builddir/make.err && dnf install alsa-lib-devel.i686
   fi
}

do_command()
{
    local j
    if [[ "$BRANCH" == "alloc" ]]
      then
        if [[ "$MACHINE" == "x86_64" ]]
          then
            if [[ "$OSTYPE" == "cygwin" ]]
              then
                  source "case-alloc-64-cygwin.sh"
              else
                  source "case-alloc-64.sh"
            fi
          else
            if [[ "$OSTYPE" == "cygwin" ]]
              then
                  source "case-alloc-32-cygwin.sh"
              else
                  source "case-alloc-32.sh"
            fi
        fi
      else # not alloc (so master)
        if [[ "$MACHINE" == "x86_64" ]]
          then
            source "case-master-64.sh"
          else
            source "case-master-32.sh"
        fi
    fi
}

if [[ "$1" = "" ]]; then
  COLUMNS=160
  select opt in "${options[@]}" "Exit"
    do
      for i in $REPLY
      do
          do_command $i
      done
      break
    done
else
    do_command $1
fi

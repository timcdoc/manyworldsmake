#autogenerated from awk -f gen-alloc-build-sh.awk
case "$1" in
1) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ARRAYALLOC=1  MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
2) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ARRAYALLOC=1  MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
3) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_TOMSARRAY=1 MAKE_ARRAYALLOC=1  MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
4) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_TOMSARRAY=1 MAKE_ARRAYALLOC=1  MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
5) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
6) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_DEBUG=debug    ; make_wrapper;;
7) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
8) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
9) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_STRIP=1    ; make_wrapper;;
10) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
11) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
12) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_DEBUG=debug    ; make_wrapper;;
13) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
14) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
15) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_STRIP=1    ; make_wrapper;;
16) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
17) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
18) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug    ; make_wrapper;;
19) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
20) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
21) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_STRIP=1    ; make_wrapper;;
22) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
23) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
24) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug    ; make_wrapper;;
25) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
26) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
27) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_STRIP=1    ; make_wrapper;;
28) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
29) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
30) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_DEBUG=debug    ; make_wrapper;;
31) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
32) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
33) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_STRIP=1    ; make_wrapper;;
34) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
35) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
36) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_DEBUG=debug    ; make_wrapper;;
37) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
38) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
39) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_STRIP=1    ; make_wrapper;;
40) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
41) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
42) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug    ; make_wrapper;;
43) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
44) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
45) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1    ; make_wrapper;;
46) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
47) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
48) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug    ; make_wrapper;;
49) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
50) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
51) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1    ; make_wrapper;;
52) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
53) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
54) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug    ; make_wrapper;;
55) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
56) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
57) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_STRIP=1    ; make_wrapper;;
58) unset $unsetARGS; export MAKE_BITNESS=32 MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
59) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ARRAYALLOC=1  MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
60) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ARRAYALLOC=1  MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
61) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_TOMSARRAY=1 MAKE_ARRAYALLOC=1  MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
62) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_TOMSARRAY=1 MAKE_ARRAYALLOC=1  MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
63) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
64) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_DEBUG=debug    ; make_wrapper;;
65) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
66) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
67) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_STRIP=1    ; make_wrapper;;
68) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
69) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
70) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_DEBUG=debug    ; make_wrapper;;
71) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
72) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
73) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_STRIP=1    ; make_wrapper;;
74) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
75) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
76) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug    ; make_wrapper;;
77) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
78) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
79) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_STRIP=1    ; make_wrapper;;
80) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZATOMS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
81) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
82) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug    ; make_wrapper;;
83) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
84) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
85) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_STRIP=1    ; make_wrapper;;
86) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ZSTRINGS=1 MAKE_ZATOMS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
87) unset $unsetARGS; export MAKE_BRICK=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
88) unset $unsetARGS; export MAKE_BRICK=1 MAKE_DEBUG=debug    ; make_wrapper;;
89) unset $unsetARGS; export MAKE_BRICK=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
90) unset $unsetARGS; export MAKE_BRICK=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
91) unset $unsetARGS; export MAKE_BRICK=1 MAKE_STRIP=1    ; make_wrapper;;
92) unset $unsetARGS; export MAKE_BRICK=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
93) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
94) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_DEBUG=debug    ; make_wrapper;;
95) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
96) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
97) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_STRIP=1    ; make_wrapper;;
98) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
99) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
100) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug    ; make_wrapper;;
101) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
102) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
103) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1    ; make_wrapper;;
104) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
105) unset $unsetARGS; export MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
106) unset $unsetARGS; export MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug    ; make_wrapper;;
107) unset $unsetARGS; export MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
108) unset $unsetARGS; export MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
109) unset $unsetARGS; export MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1    ; make_wrapper;;
110) unset $unsetARGS; export MAKE_BRICK=1 MAKE_TYPELESS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
111) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug MAKE_X11=1  ; make_wrapper;;
112) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug    ; make_wrapper;;
113) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_DEBUG=debug   MAKE_STHREAD=1; make_wrapper;;
114) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_STRIP=1 MAKE_X11=1  ; make_wrapper;;
115) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_STRIP=1    ; make_wrapper;;
116) unset $unsetARGS; export MAKE_BRICK_ATOMIC=1 MAKE_TYPELESS=1 MAKE_STRIP=1   MAKE_STHREAD=1; make_wrapper;;
117) make clean-all-builds
for j in  2 4 8 9 10 14 15 16 20 21 22 26 27 28 32 33 34 38 39 40 44 45 46 50 51 52 56 57 58 60 62 66 67 68 72 73 74 78 79 80 84 85 86 90 91 92 96 97 98 102 103 104 108 109 110 114 115 116 2 4 8 9 10 14 15 16 20 21 22 26 27 28 32 33 34 38 39 40 44 45 46 50 51 52 56 57 58 60 62 66 67 68 72 73 74 78 79 80 84 85 86 90 91 92 96 97 98 102 103 104 108 109 110 114 115 116
do
do_command $j
done;;
118) for j in  1 2 3 4 6 9 12 15 18 21 24 27 30 33 36 39 42 45 48 51 54 57 59 60 61 62 63 64 66 67 69 70 72 73 75 76 78 79 81 82 84 85 87 88 90 91 93 94 96 97 99 100 102 103 105 106 108 109 111 112 114 115 1 2 3 4 5 6 8 9 11 12 14 15 17 18 20 21 23 24 26 27 29 30 32 33 35 36 38 39 41 42 44 45 47 48 50 51 53 54 56 57 59 60 61 62 63 64 66 67 69 70 72 73 75 76 78 79 81 82 84 85 87 88 90 91 93 94 96 97 99 100 102 103 105 106 108 109 111 112 114 115
do
do_command $j
done;;
esac

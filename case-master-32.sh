            case "$1" in 
                1) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUG=debug; make_wrapper;;
                2) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUG=debug MAKE_STHREAD=1; make_wrapper;;
                3) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1; make_wrapper;;
                4) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1 MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                5) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_BASE=1; make_wrapper;;
                6) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_BASE=1 MAKE_STHREAD=1; make_wrapper;;
                7) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                8) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG=debug; make_wrapper;;
                9) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1; make_wrapper;;
                10) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG=debug MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                11) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1 MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                12) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1; make_wrapper;;
                13) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                14) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ARRAYALLOC=1 MAKE_BITNESS=32 MAKE_DEBUG=debug MAKE_STHREAD=1; make_wrapper;;
                15) unset $unsetARGS; export MAKE_NOBRICK=1 MAKE_ARRAYALLOC=1 MAKE_BITNESS=32 MAKE_BASE=1 MAKE_STHREAD=1; make_wrapper;;
                16) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUG=debug; make_wrapper;;
                17) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_STHREAD=1 MAKE_DEBUG=debug; make_wrapper;;
                18) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1; make_wrapper;;
                19) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1 MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                20) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BASE=1 MAKE_BITNESS=32; make_wrapper;;
                21) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BASE=1 MAKE_BITNESS=32 MAKE_STHREAD=1; make_wrapper;;
                22) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                23) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG=debug; make_wrapper;;
                24) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1; make_wrapper;;
                25) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG=debug MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                26) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG=debug MAKE_DEBUG_MEMORY=1 MAKE_DEBUGLISPSTACK=1; make_wrapper;;
                27) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 ; make_wrapper;;
                28) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG_MEMORY=1; make_wrapper;;
                29) unset $unsetARGS; export MAKE_TYPELESS=1 MAKE_NOBRICK=1 MAKE_BITNESS=32 MAKE_ALSA=1 MAKE_X11=1 MAKE_DEBUG_MEMORY=1 MAKE_DEBUGLISPSTACK=1; make_wrapper;;
            esac
   

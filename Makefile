#build.sh is heavily used, and calls back into this.
#build.sh and Makefile can be run standalone, but you'll have to do the same logic to decide which defines and flags, as is done here.
DATETIME:=$(shell date +"%Y%m%d%H%M%S")
MACHINE=$(shell uname -m)
OSTYPE=$(shell bash -c "( [ -z "\$$OSTYPE" ] && echo 'windows_nt' ) || echo \$$OSTYPE | sed 's/-.*//'")
NATIVE_ARCH=$(shell bash -c "gcc -v 2>&1 | grep Target: | sed -e 's/.*: //' -e 's/-.*//'")
MAKE_DEBUG?=rel
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)
BINTARGBASE=../build
BINTARG=$(BINTARGBASE)/$(BRANCH)$(MAKE_EXT)
#SUDOCP=yes | cp -f 
#EXEBINTARG=$(HOME)/.local/bin/lisp

ifeq ($(BRANCH),alloc)
  ifeq ($(NATIVE_ARCH),x86_64)
    SUDOCP=cp -f 
    EXEBINTARG=/usr/bin/lisp 
    ifeq ($(MAKE_BITNESS),32)
      ARCH=i486
      CFLAGS=-march=i686 -mcx16 -latomic -m32 -DBRANCH=1
      LDFLAGS= -m32 -lm -ldl -lpthread -rdynamic -lrt
      LDBASEFLAGS= -m32 -lm
    else
      CFLAGS=-march=x86-64 -mcx16 -latomic -DBRANCH=1
      ARCH=x86_64
      LDFLAGS= -lm -ldl -lpthread -rdynamic -lrt
      LDBASEFLAGS= -lm
    endif
    #CFLAGS= $(CFLAGS) -Wa,-adhln -g -DBRANCH=1
  else
    ifeq ($(NATIVE_ARCH),arm)
      SUDOCP=sudo cp -f 
      EXEBINTARG=/usr/bin/lisp 
      ARCH=$(MACHINE)-32
      CFLAGS=-DBRANCH=1
      LDFLAGS= -lm -ldl -lpthread -rdynamic -lrt
      LDBASEFLAGS= -lm
    else
      SUDOCP=cp -f 
      EXEBINTARG=/usr/bin/lisp 
      ARCH=i486
      CFLAGS=-DBRANCH=1
      LDFLAGS= -lm -ldl -lpthread -rdynamic -lrt
      LDBASEFLAGS= -lm
    endif
  endif
else
  ifeq ($(NATIVE_ARCH),x86_64)
    SUDOCP=cp -f 
    EXEBINTARG=/usr/bin/lisp 
    ifeq ($(MAKE_BITNESS),32)
      CFLAGS=-march=i486 -m32 -DBRANCH=0
      ARCH=i486
      LDFLAGS=-m32 -lm -ldl -lpthread -rdynamic -lrt
      LDBASEFLAGS=-m32 -lm
    else
      CFLAGS=-march=x86-64 -DBRANCH=0
      ARCH=x86_64
      LDFLAGS= -lm -ldl -lpthread -rdynamic -lrt
      LDBASEFLAGS=-lm
    endif
  else
    ifeq ($(NATIVE_ARCH),arm)
      SUDOCP=sudo cp -f 
      EXEBINTARG=/usr/bin/lisp 
      ARCH=$(MACHINE)-32
      CFLAGS=-DBRANCH=0
      LDFLAGS= -lm -ldl -lpthread -rdynamic -lrt
      LDBASEFLAGS= -lm
    else
      SUDOCP=cp -f 
      EXEBINTARG=/usr/bin/lisp 
      ARCH=$(NATIVE_ARCH)
      CFLAGS=-DBRANCH=0
      LDFLAGS= -m32 -lm -ldl -lpthread -rdynamic -lrt
      LDBASEFLAGS= -m32 -lm
    endif
  endif
endif

ifeq ($(OSTYPE),cygwin)
  ifeq ($(MAKE_BITNESS),32)
    CC=echo fake i686-pc-cygwin-gcc
    LD=echo fake i686-pc-cygwin-gcc 
    AR=i686-pc-cygwin-ar 
    LIBS=-L/usr/i686-pc-cygwin/sys-root/usr/lib
  else
    CC=echo fake gcc
    LD=echo fake gcc 
    AR=echo fake ar 
    LIBS=-L/usr/lib
  endif
else
  CC=echo fake gcc
  LD=echo fake gcc 
  AR=echo fake ar 
  LIBS=-L/usr/lib
endif

SRC1=src
SRC3=ratpak

OBSRC1=$(BINTARG)/$(OSTYPE)/$(ARCH)/$(MAKE_DEBUG)$(MAKE_SUBBUILD)/$(SRC1)
OBSRC3=$(BINTARG)/$(OSTYPE)/$(ARCH)/$(MAKE_DEBUG)$(MAKE_SUBBUILD)/$(SRC3)
PROGSRC1=$(BINTARG)/$(OSTYPE)/$(ARCH)/$(MAKE_DEBUG)$(MAKE_SUBBUILD)
TARGSRC=$(BINTARG)/$(OSTYPE)/$(ARCH)/$(MAKE_DEBUG)$(MAKE_SUBBUILD)

OBJS2=$(OBSRC2)/genstate.o

SRCS1=$(SRC1)/lisp.c $(SRC1)/carcdr.c $(SRC1)/read_lisp.c $(SRC1)/eval_lisp.c $(SRC1)/print_lisp.c $(SRC1)/stack.c $(SRC1)/string_ext.c $(SRC1)/hashfunc.c $(SRC1)/builtins.c $(SRC1)/builtin_c.c $(SRC1)/builtin_m.c $(SRC1)/builtin_dl.c $(SRC1)/builtin_s.c $(SRC1)/builtin_p.c $(SRC1)/sfstream.c $(SRC1)/rathooks.c $(SRC1)/atoms-and-strings.c

# $(SRC1)/atom_from_value.c

OBJS1=$(OBSRC1)/lisp.o $(OBSRC1)/carcdr.o $(OBSRC1)/read_lisp.o $(OBSRC1)/eval_lisp.o $(OBSRC1)/print_lisp.o $(OBSRC1)/stack.o $(OBSRC1)/string_ext.o $(OBSRC1)/hashfunc.o $(OBSRC1)/builtins.o $(OBSRC1)/builtin_c.o $(OBSRC1)/builtin_m.o $(OBSRC1)/builtin_dl.o $(OBSRC1)/builtin_s.o $(OBSRC1)/builtin_p.o $(OBSRC1)/sfstream.o $(OBSRC1)/rathooks.o $(OBSRC1)/genstate.o $(OBSRC1)/atoms-and-strings.o

# $(OBSRC1)/atom_from_value.o

OPTOBJS= $(OBSRC1)/brick.o $(OBSRC1)/music.o $(OBSRC1)/builtin_x.o $(OBSRC1)/film.o $(OBSRC1)/builtin_sdl.o $(OBSRC1)/builtin_raw.o $(OBSRC1)/garbage.o $(OBSRC1)/alloc.o

OBJS3=$(OBSRC3)/rat.o $(OBSRC3)/num.o $(OBSRC3)/conv.o $(OBSRC3)/itrans.o $(OBSRC3)/trans.o $(OBSRC3)/transh.o $(OBSRC3)/itransh.o $(OBSRC3)/support.o $(OBSRC3)/logic.o $(OBSRC3)/exp.o $(OBSRC3)/fact.o $(OBSRC3)/basex.o

ifeq ($(BRANCH),alloc)
SRCS1+= $(SRC1)/brick.c
OBJS1+= $(OBSRC1)/brick.o
endif

ifdef MAKE_DEBUG_MEMORY
  SRCS1+= $(SRC1)/suballoc.c
  OBJS1+= $(OBSRC1)/suballoc.o
endif

PROG1=$(PROGSRC1)/lisp
#TARGLIB=$(TARGSRC)/libratpak.a

SRCS3=$(SRC3)/rat.c $(SRC3)/num.c $(SRC3)/conv.c $(SRC3)/itrans.c $(SRC3)/trans.c $(SRC3)/transh.c $(SRC3)/itransh.c $(SRC3)/support.c $(SRC3)/logic.c $(SRC3)/exp.c $(SRC3)/fact.c $(SRC3)/basex.c

INCLUDES= -I$(GCCINC) -I/usr/include -Iinc -D_REENTRANT 

ifdef MAKE_X11
  ifdef MAKE_DEBUG_MEMORY
      MAKE_SUBBUILD:=-mem-full
    else
      MAKE_SUBBUILD:=-full
      $(SRCS1)+=$(SRC1)/music.c
      $(OBJS1)+=$(OBSRC1)/music.o
  endif
else
  ifdef MAKE_ALSA
    $(SRCS1)+=$(SRC1)/music.c
    $(OBJS1)+=$(OBSRC1)/music.o
    ifdef MAKE_DEBUG_MEMORY
      MAKE_SUBBUILD:=-mem-full
    else
      MAKE_SUBBUILD:=-full
    endif
  else
    ifdef MAKE_DEBUG_MEMORY
      MAKE_SUBBUILD:=-mem-base
    else
      ifdef MAKE_STHREAD
        MAKE_SUBBUILD:=-base-st
      else
        MAKE_SUBBUILD:=-base
      endif
    endif
  endif
endif

ifeq ($(MAKE_DEBUG),rel)
  ifdef MAKE_STRIP
    CFLAGS+= -std=gnu11 -Os -pthread -rdynamic $(INCLUDES)
    CONDITIONALSTRIP=strip $(PROG1)
  else
    CFLAGS+= -std=gnu11 -O3 -pthread -rdynamic $(INCLUDES)
  endif
else
  #Adding -pg for profiling under any debug
  ifeq ($(MAKE_DEBUG),0)
    MAKE_DEBUG:=debug
    CFLAGS+= -std=gnu11 -O0 -pg -g -ggdb -lpthread -pthread -rdynamic $(INCLUDES) -DDEBUG_NO_INLINE
    LDFLAGS+= -pg 
  else
    ifeq ($(MAKE_DEBUG),debug)
      MAKE_DEBUG:=debug
      #CFLAGS+= -std=gnu11 -O0 -g -ggdb -lpthread -pthread -rdynamic $(INCLUDES) -DDEBUG_NO_INLINE -DDEBUGSTATS=1
      CFLAGS+= -std=gnu11 -O0 -pg -g -ggdb -lpthread -pthread -rdynamic $(INCLUDES) -DDEBUG_NO_INLINE 
      LDFLAGS+= -pg 
    else
      CFLAGS+= -std=gnu11 -O0 -pg -g -ggdb -lpthread -pthread -rdynamic $(INCLUDES) -DDEBUG=$(MAKE_DEBUG) -DDEBUG_NO_INLINE
      LDFLAGS+= -pg 
    endif
  endif
endif

ifdef MAKE_ZSTRINGS
CFLAGS+= -DZSTRINGS=$(MAKE_ZSTRINGS)
endif

ifdef MAKE_ZATOMS
CFLAGS+= -DZATOMS=$(MAKE_ZATOMS)
endif

ifdef MAKE_DEBUG_MEMORY
CFLAGS+= -DDEBUG_MEMORY=$(MAKE_DEBUG_MEMORY)
endif

ifdef MAKE_DEFINE_DEBUG
CFLAGS+= -DDEBUG=$(MAKE_DEFINE_DEBUG)
endif

#stack stuff not implemented on cygwin that I know of.
ifdef MAKE_DEBUG_SIGSEGV
ifeq ($(OSTYPE),cygwin)
#nothingburger
else
CFLAGS+= -g -DDEBUG_SIGSEGV=1
endif
endif

ifdef MAKE_STHREAD
CFLAGS+= -DMAKE_STHREAD=1
endif

ifdef MAKE_DEBUG_BRICK
CFLAGS+= -DDEBUG_BRICK=$(MAKE_DEBUG_BRICK)
endif

ifdef MAKE_DEBUGLISPSTACK
CFLAGS+= -DDEBUGLISPSTACK=1
MAKE_DEBUG:=debug_no_timing
endif

ifdef MAKE_DEBUGSTATS
CFLAGS+= -DDEBUGSTATS=1
MAKE_DEBUG:=debug_no_timing
endif

ifdef MAKE_NOBRICK
  ifdef MAKE_TYPELESS
    CFLAGS+= -DUSE_BRICK=0 -DHLSP_TYPELESS=1
    MAKE_EXT:=-nobrick-tyless
  else
    ifdef MAKE_ARRAYALLOC
      ifdef MAKE_TOMSARRAY
        CFLAGS+= -DUSE_BRICK=0 -DMAKE_ARRAYALLOC=1 -DMAKE_TOMSARRAY=1
        MAKE_EXT:=-toms-ary
      else
        CFLAGS+= -DUSE_BRICK=0 -DMAKE_ARRAYALLOC=1
        MAKE_EXT:=-ary
      endif
    else
      CFLAGS+= -DUSE_BRICK=0
      ifdef MAKE_ZSTRINGS
        ifdef MAKE_ZATOMS
          MAKE_EXT:=-nobrick-zstr-zatm
        else
          MAKE_EXT:=-nobrick-zstr
        endif
      else
        ifdef MAKE_ZATOMS
          MAKE_EXT:=-nobrick-zatm
        else
          MAKE_EXT:=-nobrick
        endif
      endif
    endif
  endif
else
  CFLAGS+= -DUSE_BRICK=1
  ifdef MAKE_BRICK_ATOMIC
    ifdef MAKE_TYPELESS
      MAKE_EXT:=-atom-tyless
      CFLAGS+= -DBRICK_ATOMIC=$(MAKE_BRICK_ATOMIC) -DHLSP_TYPELESS=1
    else
      MAKE_EXT:=-atom
      CFLAGS+= -DBRICK_ATOMIC=$(MAKE_BRICK_ATOMIC)
    endif
  else
    ifdef MAKE_TYPELESS
      MAKE_EXT:=-brick-tyless
      CFLAGS+= -DHLSP_TYPELESS=1
    else
      MAKE_EXT:=-brick
    endif
  endif
endif

ifeq ($(OSTYPE),cygwin)
CFLAGS+= -DCYGWIN=1
endif

ifdef MAKE_ARRAYALLOC
  OBJS1+= $(OBSRC1)/fixedalloc.o
  SRCS1+= $(SRC1)/fixedalloc.c
else
  OBJS1+= $(OBSRC1)/garbage.o $(OBSRC1)/alloc.o
  SRCS1+= $(SRC1)/garbage.c $(SRC1)/alloc.c
endif

ifdef MAKE_X11
LDFLAGS+= -lX11
LIBS+= -L/usr/lib/X11 -lX11
OBJS1+= $(OBSRC1)/builtin_x.o $(OBSRC1)/film.o $(OBSRC1)/music.o
SRCS1+= $(SRC1)/builtin_x.c $(SRC1)/film.c $(SRC1)/music.c
CFLAGS+= -DMAKE_X11=1
endif

ifdef MAKE_SDL
LDFLAGS+= -lSDL2 -lSDL2_mixer -lSDL2_image -lSDL2_ttf
OBJS1+= $(OBSRC1)/builtin_sdl.o
SRCS1+= $(SRC1)/builtin_sdl.c
CFLAGS+= -DMAKE_SDL=1
endif

ifdef MAKE_RAW
OBJS1+= $(OBSRC1)/builtin_raw.o
SRCS1+= $(SRC1)/builtin_raw.c
CFLAGS+= -DMAKE_RAW=1
endif

ifdef MAKE_ALSA
LDFLAGS+= -lasound
CFLAGS+= -DMAKE_ALSA=1
endif

LISPVERSION=$(BRANCH)$(MAKE_EXT)/$(OSTYPE)/$(ARCH)/$(MAKE_DEBUG)$(MAKE_SUBBUILD) $(DATETIME)
CFLAGS+= -DLISPVERSION='"$(LISPVERSION)"'
.PHONY: all $(BINTARG) $(OBSRC1) $(OBSRC3)

build:
	@./build.sh

all: $(BINTARG) $(OBSRC1) $(OBSRC3) $(SRC1)/scan.h $(SRC1)/special_str.h $(PROG1) $(PROG2) $(TARGLIB)

echobuilddir:
	@[ -d ../build/$(BRANCH)$(MAKE_EXT)/$(OSTYPE)/$(ARCH)/$(MAKE_DEBUG)$(MAKE_SUBBUILD) ] || mkdir -p ../build/$(BRANCH)$(MAKE_EXT)/$(OSTYPE)/$(ARCH)/$(MAKE_DEBUG)$(MAKE_SUBBUILD)
	@echo ../build/$(BRANCH)$(MAKE_EXT)/$(OSTYPE)/$(ARCH)/$(MAKE_DEBUG)$(MAKE_SUBBUILD)

$(BINTARG):
	@[ -d $@ ] || mkdir -p $@

$(OBSRC1):
	@[ -d $@ ] || mkdir -p $@

$(OBSRC3):
	@[ -d $@ ] || mkdir -p $@

#$(TARGLIB): $(OBJS3)
#	$(AR) cru $(TARGLIB) $(OBJS3)
#

$(PROG1): $(OBJS1) $(OBJS3) $(USRLIBS) 
	$(LD) $(LDFLAGS) -o $(PROG1) $(OBJS1) $(OBJS3) $(USRLIBS) $(LIBS) -lpthread -lrt
	$(CONDITIONALSTRIP)
	$(SUDOCP) $(PROG1) $(EXEBINTARG)

$(OBSRC1)/%.o: $(SRC1)/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

$(OBSRC3)/%.o: $(SRC3)/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

$(PROG2): $(OBJS2) 
	$(LD) $(LDBASEFLAGS) $(LIBS) -o $@ $^

$(SRC1)/scan.h: $(SRC1)/genscan.awk
	awk -f $(SRC1)/genscan.awk

$(SRC1)/special_str.h: $(SRC1)/genscan.awk
	awk -f $(SRC1)/genscan.awk

clean-all-builds:
	rm -r ../build/

clean:
	$(RM) $(OBJS1) $(OBJS2) $(OBJS3) $(TARGLIB) $(PROG1) $(OPTOBJS)

